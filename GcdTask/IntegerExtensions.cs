﻿using System;
#pragma warning disable

namespace GcdTask
{
    public static class IntegerExtensions
    {
        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue]  by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int FindGcd(int a, int b)
        {
            if (a == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a));
            }

            if (b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(b));
            }

            if (a == 0 && b == 0)
            {
                throw new ArgumentException(string.Empty, nameof(a));
            }
            else if (a == 0) return Math.Abs(b);
            else if (b == 0) return Math.Abs(a);


            int f = a;
            int s = b;
            int r = 1;
            while (r != 0)
            {
                r = f % s;
                if (r == 0) break;
                f = s;
                s = r;
            }

            return Math.Abs(s);
        }
    }
}
